# Light Crypto Faucet

Lightweight Crypto Faucet using Python and Cherrypy to distribute test coins

## Features

 * Uses Electron Cash as backend
 * Uses Cherrypy for frontend
 * Supports multiple networks, any things supported by EC.
 * Uses home made simple math captcha
 * Dark/Light theme
 * Works with/without javascript
 * Uses [Chota](https://jenil.github.io/chota/) CSS framework

## Running with Docker

The easist way to run the faucet is by using Docker, run the following command in the directory where `Dockerfile` exist:

```bash
sudo DOCKER_BUILDKIT=1 docker build --progress=plain . --tag=faucet_image
```
It should create a docker container of the faucet. 


Create a Docker volume to store the data:

```bash
sudo docker volume create faucet_volume
```

Run the faucet:

```bash
sudo docker run -d --restart always -v faucet_volume:/home/user/data -p 3004:8080 -e network_options=testnet,testnet4,chipnet -e testnet4_rpc_user=user -e testnet_rpc_user=user -e chipnet_rpc_user=user -e web_access_log=access.log -e web_error_log=error.log faucet_image
```

Run the faucet using .env file:

```
sudo docker run -d --restart always -v faucet_volume5:/home/user/data -p 3004:8080 --env-file=.env faucet_image
```

Don't be scared. Probably I should make it shorter but it's just telling the following:

* `-d` means run in Daemon mode, otherwise it will just run in the terminal session
* `--restart always` is telling to restart the container in case of error or server shutdown
* `-v faucet_volume:/home/user/data` is to mount the internal `data` directory to the docker volume
* `-p 3004:8080` tells the container to expose the internal `8080` cherrypy port on host `3004` port
* `-e network_options=testnet,testnet4,chipnet` is specifyging the networks that the faucet will support
* other `-e` options are just setting env variables required to run the faucet


## Screenshot

![](./Bitcoin_Cash_Testnet_Faucet_2023_01_01.png)

## Info Section

The faucet page is very simple, people can see more information if they look at the Info section. 

Info section is created from `info.md` file.
