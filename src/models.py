from peewee import (
    Model,
    SqliteDatabase,
    AutoField,
    TimestampField,
    CharField,
    BooleanField,
    DecimalField,
)
from peewee import IntegrityError, OperationalError
from datetime import datetime, timedelta
import os

from dotenv import load_dotenv

load_dotenv(os.getenv("ENV_FILE"))

try:
    database_file = os.getenv("database_file")
except KeyError:
    exit("Unable to get database_file from environment variables")

# Time gap to determine if user is recent
time_gap = os.getenv("time_gap")

# Using Peewee Run-time database configuration
# https://docs.peewee-orm.com/en/latest/peewee/database.html#run-time-database-configuration
db = SqliteDatabase(None)


class Transactions(Model):
    """The database class"""

    _id = AutoField(unique=True)
    session_id = CharField(max_length=60)
    ip = CharField(max_length=30)
    user_address = CharField(max_length=60)
    coin_type = CharField(max_length=10)
    amount_sent = DecimalField()
    success = BooleanField()
    tx_id = CharField(max_length=60)
    date = TimestampField()

    class Meta:
        database = db  # This model uses the "db" database.


def create_table(database_file):
    db.init(database_file)
    Transactions.create_table()


def save_transaction(
    session_id, ip, user_address, coin_type, amount, transaction_status, tx_id
):
    """Checks if a Telegram user is present in the database.
    Returns True if a user is created, False otherwise.
    """
    db.connect(reuse_if_open=True)
    try:
        Transactions.create(
            session_id=session_id,
            ip=ip,
            user_address=user_address,
            coin_type=coin_type,
            amount_sent=amount,
            date=datetime.now(),
            success=transaction_status,
            tx_id=tx_id,
        )
        db.close()
        return True
    except IntegrityError:
        db.close()
        raise
    except OperationalError:
        db.close()
        raise


def recent_user(session_id, ip, user_address, coin_type, check_ip=True):
    """check if the user have already requested the coin before"""
    db.connect(reuse_if_open=True)
    try:
        # intervals allowed between requests in hours
        cutoff = datetime.now() - timedelta(hours=int(time_gap))
        # Check if in the sepcified period, the user have requested coins
        # more than one time, based on their session ID, IP address, coin address
        if check_ip:
            query = Transactions.select().where(
                (Transactions.date >= cutoff)
                & (Transactions.coin_type == coin_type)
                & (
                    (Transactions.session_id == session_id)
                    | (Transactions.ip == ip)
                    | (Transactions.user_address == user_address)
                )
            )
        else:
            # If ip check is not required
            query = Transactions.select().where(
                (Transactions.date >= cutoff)
                & (Transactions.coin_type == coin_type)
                & (
                    (Transactions.session_id == session_id)
                    | (Transactions.user_address == user_address)
                )
            )
        if query.exists():
            return True
        else:
            return False
    except IntegrityError:
        db.close()
        raise
