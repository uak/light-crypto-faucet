#!/usr/bin/env python3

import os
import json
import time
import pickle
import logging
import cherrypy
import markdown

from captcha.image import ImageCaptcha

# Needed when dropping privileges after privileged run for high port and ssl read
# Allow running as deamon
# ~ from cherrypy.process.plugins import Daemonizer
# Allows continue running as a user with less privileges
# ~ from cherrypy.process.plugins import DropPrivileges
# Allow creating PID file to monitor process
# ~ from cherrypy.process.plugins import PIDFile
# Allow running on other ports
# ~ from cherrypy._cpserver import Server

from decimal import Decimal

# Import from the database models
from models import save_transaction, recent_user, create_table

# Import wallet functions
from ec_slp_lib import EcSLP

from random import randint, choice

# Using .env file
from dotenv import load_dotenv

# Library to fix issue with creating wallets without user interaction
# https://github.com/Electron-Cash/Electron-Cash/issues/2341
from create_wallet_pexpect import create_wallet_pexpect

from helpers import get_env_boolean

# Get location of .env file
load_dotenv(os.getenv("ENV_FILE"))

# Enable logging
logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
    level=logging.DEBUG,  # DEBUG #INFO
)

logger = logging.getLogger(__name__)


# Creaste instance of EcSLP
ec_slp = EcSLP()

# Initiate database and create table
create_table(os.getenv("database_file"))


config_dict = {
    "python_path": os.getenv("python_path"),
    "electron_cash_path": os.getenv("electron_cash_path"),
    "data_dir": os.getenv("data_dir"),
    "custom_dir": os.getenv("custom_dir"),
    "use_custom_dir": os.getenv("use_custom_dir"),
    "networks": os.getenv("network_options"),
    "network_options": os.getenv("network_options").split(","),
    "addresses_file": os.getenv("addresses_file"),
    "environment": os.getenv("environment"),
    "web_access_log": os.getenv("web_access_log"),
    "web_error_log": os.getenv("web_error_log"),
    "cherrypy_conf": os.getenv("cherrypy_conf"),
    "token_support": get_env_boolean("token_support"),
    "api_url": os.getenv("api_url"),
    "mainnet": {
        "user": os.getenv("mainnet_rpc_user"),
        "url": os.getenv("mainnet_rpc_url"),
        "port": os.getenv("mainnet_rpc_port"),
        "password": "",
        "wallet_path": os.getenv("mainnet_wallet_path"),
        "explorer": os.getenv("mainnet_explorer"),
    },
    "testnet4": {
        "user": os.getenv("testnet4_rpc_user"),
        "url": os.getenv("testnet4_rpc_url"),
        "port": os.getenv("testnet4_rpc_port"),
        "password": "",
        "wallet_path": os.getenv("testnet4_wallet_path"),
        "explorer": os.getenv("testnet4_explorer"),
    },
    "testnet": {
        "user": os.getenv("testnet_rpc_user"),
        "url": os.getenv("testnet_rpc_url"),
        "port": os.getenv("testnet_rpc_port"),
        "password": "",
        "wallet_path": os.getenv("testnet_wallet_path"),
        "explorer": os.getenv("testnet_explorer"),
    },
    "chipnet": {
        "user": os.getenv("chipnet_rpc_user"),
        "url": os.getenv("chipnet_rpc_url"),
        "port": os.getenv("chipnet_rpc_port"),
        "password": "",
        "wallet_path": os.getenv("chipnet_wallet_path"),
        "explorer": os.getenv("chipnet_explorer"),
    },
}

addresses_file = config_dict["addresses_file"]

# Create wallet for each enabled network if does not exist
for network in config_dict["network_options"]:
    wallet_path = config_dict[network]["wallet_path"]
    logger.info(f"wallet path {wallet_path}")
    logger.debug(f"wallet path {wallet_path}")
    if os.path.isfile(wallet_path):
        pass
        os.path.isfile(wallet_path)
        logger.info(f"wallet path {wallet_path} exist as file")
    else:
        python_path = os.getenv("python_path")
        ec_path = os.getenv("electron_cash_path")
        data_dir = os.getenv("custom_dir")
        create_wallet_pexpect(python_path, ec_path, data_dir, network)
        logger.debug(f"wallet created for network {network}")


# Get configuration data from Electron Cash config file for each network
for network in config_dict["network_options"]:
    try:
        if network != "mainnet":
            config_dict["network"] = network
            ec_slp.set_config_data(config_dict, "rpcport", config_dict[network]["port"])
        else:
            config_dict["network"] = "mainnet"
            print(f"{config_dict=}")
            ec_slp.set_config_data(config_dict, "rpcport", config_dict[network]["port"])

        # ~ ec_slp.set_config_data(config_dict, "rpcport", config_dict[network]["port"])
        logger.debug(f'got config data {network} port {config_dict[network]["port"]} ')
        rpc_password = ec_slp.get_config_data(config_dict, "rpcpassword")
        config_dict[network]["password"] = rpc_password
        logger.debug(
            f"got password from config data for {network} with length {len(rpc_password)}"
        )
        # logger.debug(f"RPC version: {ec_slp.ec_rpc(config_dict[network], 'version')}")
    except Exception as e:
        logger.error(f"getting config: Exception: {e}")
        raise
        exit("Unable to get or set config from config file")


# Start daemon and load wallet for each enabled network
for network in config_dict["network_options"]:
    try:
        config_dict["network"] = network
        ec_slp.start_daemon(config_dict)
        logger.info(f"Daemon for network {network} started")
        time.sleep(2)
        if len(rpc_password) == 0:
            config_dict["network"] = network
            logger.debug("Restarting daemon to set password in config file")
            ec_slp.start_daemon(config_dict)
            time.sleep(2)
            ec_slp.load_wallet(config_dict, config_dict[network]["wallet_path"])
            time.sleep(2)
            ec_slp.stop_daemon(config_dict)
            config_dict[network]["password"] = ec_slp.get_config_data(
                config_dict, "rpcpassword"
            )
            logger.debug("Daemon should be stopped now")
            time.sleep(2)
            ec_slp.start_daemon(config_dict)
        ec_slp.load_wallet(config_dict, config_dict[network]["wallet_path"])
        logger.info(f"wallet {config_dict[network]['wallet_path']} loaded")
    except Exception as e:
        logger.info(f"Could not start some daemon \n:{e}")
        exit("daemon is not running or unable to load wallet")


# Check daemon status for each wallet
for network in config_dict["network_options"]:
    config_dict["network"] = network
    try:
        daemon_running = ec_slp.check_daemon(config_dict)
        logger.info(f"Daemon for network {network} is running")
    except Exception as e:
        logger.info(f"could not check daemon running {e}")
        exit("daemon is not running")

# Check that wallets are loaded
for network in config_dict["network_options"]:
    config_dict["network"] = network
    try:
        wallet_loaded = ec_slp.check_wallet_loaded(
            config_dict, config_dict[network]["wallet_path"]
        )
        logger.info(f"Wallet for {network} is loaded")
    except Exception as e:
        logger.info(f"Some wallets are not loaded\n {e}")
        exit("Some wallets are not loaded")


# Check RPC connection for each network and check balance
for network in config_dict["network_options"]:
    try:
        logger.debug(f"Network: {network}, RPC info: {config_dict[network]}")

        bch_balance = ec_slp.get_bch_balance(config_dict[network])
        if os.getenv("allow_zero_balance") is False:
            min_allowed_balance = os.getenv("min_allowed_balance")
            if Decimal(bch_balance) >= Decimal(min_allowed_balance):
                pass
            else:
                logger.info(f"No balance: {bch_balance}")
                exit("No Balance!")
        logger.info(f"Balance for network {network} is {bch_balance}")
    except Exception as e:
        logger.error(f"No balance: {e}")
        raise
        exit("Error connecting to RPC provider")


# Function to create wallet addresses and store in pickle file
def store_addresses_pickle(address_file, params):
    wallet_address = ec_slp.get_unused_bch_address(params)
    with open(addresses_file, "wb+") as handle:
        pickle.dump(wallet_address, handle)
        logger.debug(f"wallet addresses {wallet_address} saved.")


# Get unused address for each network
if os.path.isfile(addresses_file):
    logger.debug(f"Addresses file exist: {addresses_file}")
    with open(addresses_file, "rb") as handle:
        try:
            wallet_addresses = pickle.load(handle)
            logger.debug(f"wallet addresses {wallet_addresses}")
        except EOFError:
            logger.debug("Unable to load pickle file, empty?")
            store_addresses_pickle(addresses_file, config_dict[network])
        except Exception as e:
            raise e
else:
    wallet_addresses = {}
    logger.debug("Getting addresses for activated networks")
    try:
        for network in config_dict["network_options"]:
            logger.debug(f"Current network: {network} .")
            store_addresses_pickle(addresses_file, config_dict[network])

    except Exception as e:
        logger.info(f"could not get address\n {e}")


# Signs to be used in the sum math to increase captcha difficulty
plus_signs = ("➕", "+", "plus", "＋", "﹢")
random_from_tuple = choice(plus_signs)


# Check if enviroment is production, useful to disable recent user check
if config_dict["environment"] == "production":
    is_development = False
    logger.info("Production environment")
else:
    is_development = True
    logger.info("Development environment")

# Register hanler for ssl checking
# Check if connecting through ssl, redirect if not
# ~ @cherrypy.tools.register('before_handler')
# ~ def check_ssl():
# ~ if cherrypy.request.scheme == http_protocol:
# ~ pass
# ~ else:
# ~ raise cherrypy.HTTPRedirect(redirect_target, redirect_type)


explorer = {
    "mainnet": os.getenv("mainnet_explorer"),
    "testnet": os.getenv("testnet_explorer"),
    "testnet4": os.getenv("testnet4_explorer"),
    "chipnet": os.getenv("chipnet_explorer"),
}


def show_network_options():
    network_options_html = "<legend>Network</legend>"
    for network in config_dict["network_options"]:
        network_options_html = (
            network_options_html
            + f"""
        <label for="radio1">
        <input id="radio1" name="network" type="radio" class="radio" value="{network}"> {network} </label>
        """
        )
    if config_dict["token_support"]:
        logger.debug(
            f"show_network_options: With token support: {config_dict['token_support']}"
        )
        tokens_section = """<label for="token">
                    <input id="token" name="tokens" type="checkbox" class="checkbox" value="testnet" disabled=""> Get token </label>
                  <label title="Not implemented">🛈</label>"""
        network_options_html = network_options_html + tokens_section
    return network_options_html


class Form:
    """Form which the user enters his wallet address, and is shown a
    random math captcha to solve.
    """
    image = ImageCaptcha()
    faucet_active = True
    tbch_amount = os.getenv("tbch_amount")
    bch_amount = os.getenv("bch_amount")
    page_h1 = os.getenv("page_h1")
    top_logo = os.getenv("top_logo")
    sponsor_title = os.getenv("sponsor_title")
    sponsor_logo = os.getenv("sponsor_logo")
    notice_message = os.getenv("notice_message")
    custom_style = os.environ.get("custom_style", "")

    def gen_random(self):
        """Generate random numbers for the math captcha"""
        cherrypy.session["a"] = randint(0, 10)
        cherrypy.session["b"] = randint(0, 10)

    # ~ @cherrypy.tools.check_ssl()
    @cherrypy.expose
    def index(self):
        explorer_json = json.dumps(explorer)
        # ~ base_url = cherrypy.request.base
        self.gen_random()
        index = (
            open("index.html")
            .read()
            .format(
                favicon=os.getenv("favicon"),
                c=cherrypy.session["a"],
                d=cherrypy.session["b"],
                g=choice(plus_signs),
                base_url=os.getenv("base_url"),
                network=network,
                page_h1=self.page_h1,
                meta_description=os.getenv("meta_description"),
                donate=os.getenv("donate"),
                donate_tbch=os.getenv("donate_tbch"),
                og_image=os.getenv("og_image"),
                page_title=os.getenv("page_title"),
                explorer=explorer_json,
                notice_message=self.notice_message,
                network_options=show_network_options(),
                hide_donation_section=os.environ.get("donation_section_state", ""),
                hide_info_section=os.environ.get("info_section_state", ""),
                hide_sponsor=os.environ.get("hide_sponsor", ""),
                hide_top_logo=os.environ.get("hide_top_logo", ""),
                top_logo=self.top_logo,
                sponsor_title=self.sponsor_title,
                sponsor_logo=self.sponsor_logo,
                custom_style=self.custom_style,
            )
        )
        return index

    @cherrypy.expose
    def captcha(self):
        # add your own logic to generate the code
        code = str(cherrypy.session["a"]) + " + " + str(cherrypy.session["b"])
        data = self.image.generate(code)
        cherrypy.response.headers['Content-Type'] = 'image/png'
        return data

    # index.exposed=True
    @cherrypy.expose
    @cherrypy.tools.json_out()
    def greet(self, network, address=None, captcha=None):
        """
        Function to check the verify captcha and verify entered address
        """
        if not self.faucet_active:
            return {"error": "Faucet not active!"}
        rpc = config_dict[network]
        try:
            result = cherrypy.session["a"] + cherrypy.session["b"]
        except Exception as e:
            logger.warning(f"greet: exception {e}")
            return {"error": "Captcha not solved!"}

        # Verify address format and captcha
        try:
            address_valid = ec_slp.validate_address(rpc, address)
        except Exception as e:
            logger.error(f"greet: exception {e}")
            return {"error": "Unable to validate address"}

        # Check address length and validity then captcha
        if len(address) <= 69 and address_valid and (int(captcha) == result):
            recipient_address = address
            global tx_id
            only_mainnet = get_env_boolean("only_mainnet")

            if address.startswith("bitcoincash") or only_mainnet:
                logger.info("greet: using mainnet")
                # Store coin_type in the database as tbch-network where network is
                # network as testnet or testnet4 etc. (e.g: tbch-testnet4)
                coin_type = "bch" + "-" + network
                _recent_user = recent_user(
                    cherrypy.session._id,
                    cherrypy.request.remote.ip,
                    recipient_address,
                    coin_type,
                    check_ip=get_env_boolean("check_ip"),
                )
                # Check if user have requested tokens before
                if (not _recent_user) or is_development:
                    amount = self.bch_amount
                    try:
                        tx_hex = ec_slp.prepare_bch_transaction(
                            rpc,
                            recipient_address,
                            amount,
                        )
                        logger.info(f"Transaction hex is {tx_hex}")
                    except Exception as e:
                        return {"error": f"could not create transaction, Exception {e}"}

                    # Brodacast transaction using EC SLP lib
                    try:
                        tx_id = ec_slp.broadcast_tx(rpc, tx_hex["hex"])
                    except TypeError as e:
                        return {"error": f"Not enough fund, Exception: {e}"}
                    except Exception as e:
                        return {"error": f"Broadcast error: {e}"}

                    transaction_status = 1
                    # Reset the captcha so same result number can't be used
                    self.gen_random()
                    logger.info(f"Transaction id is {tx_id[1]}")
                    # save transaction to the database
                    save_transaction(
                        cherrypy.session._id,
                        cherrypy.request.remote.ip,
                        recipient_address,
                        coin_type,
                        amount,
                        transaction_status,
                        tx_id,
                    )
                    return {
                        "tx_id": tx_id[1],
                        "amount": amount,
                        "coin_type": coin_type,
                        "network": network,
                    }
                else:
                    msg = f'You can not withdrown now, try after {os.getenv("time_gap")} hours'
                    return {"error": msg}

            if address.startswith("bchtest"):
                # Store coin_type in the database as tbch-network where network is
                # network as testnet or testnet4 etc. (e.g: tbch-testnet4)
                logger.info("greet: using a testnet")
                coin_type = "tbch" + "-" + network
                _recent_user = recent_user(
                    cherrypy.session._id,
                    cherrypy.request.remote.ip,
                    recipient_address,
                    coin_type,
                    check_ip=os.getenv("check_ip"),
                )
                # Check if user have requested tokens before
                if (not _recent_user) or is_development:
                    amount = self.tbch_amount
                    try:
                        tx_hex = ec_slp.prepare_bch_transaction(
                            rpc,
                            recipient_address,
                            amount,
                        )
                        logger.info(f"Transaction hex is {tx_hex}")
                    except Exception as e:
                        return {"error": f"could not create transaction, Exception {e}"}

                    # Brodacast transaction using EC SLP lib
                    try:
                        tx_id = ec_slp.broadcast_tx(rpc, tx_hex["hex"])
                    except TypeError as e:
                        return {"error": f"Not enough fund, Exception: {e}"}
                    except Exception as e:
                        return {"error": f"Broadcast error: {e}"}

                    transaction_status = 1
                    # Reset the captcha so same result number can't be used
                    self.gen_random()
                    logger.info(f"Transaction id is {tx_id[1]}")
                    # save transaction to the database
                    save_transaction(
                        cherrypy.session._id,
                        cherrypy.request.remote.ip,
                        recipient_address,
                        coin_type,
                        amount,
                        transaction_status,
                        tx_id,
                    )
                    return {
                        "tx_id": tx_id[1],
                        "amount": amount,
                        "coin_type": coin_type,
                        "network": network,
                    }
                else:
                    msg = f'You can not withdrown now, try after {os.getenv("time_gap")} hours'
                    return {"error": msg}

            elif address.startswith("slptest"):
                coin_type = "sTST"
                # Check if user have requested tokens before, checks against
                # session, ip and address
                if not recent_user(
                    cherrypy.session._id,
                    cherrypy.request.remote.ip,
                    recipient_address,
                    coin_type,
                    check_ip=os.getenv("check_ip"),
                ):
                    # Prepare transaction using EC SLP lib
                    token_id_hex = os.getenv("token_id_hex")
                    token_amount = os.getenv("token_amount")
                    tx_hex = ec_slp.prepare_slp_transaction(
                        rpc,
                        ec_slp.wallet_path,
                        token_id_hex,
                        recipient_address,
                        token_amount,
                    )

                    # Brodacast transaction using EC SLP lib
                    tx_id = ec_slp.broadcast_tx(rpc, tx_hex)
                    self.gen_random()
                    transaction_status = 1
                    logger.info(f"Transaction id is {tx_id}")
                    save_transaction(
                        cherrypy.session._id,
                        cherrypy.request.remote.ip,
                        recipient_address,
                        coin_type,
                        amount,
                        transaction_status,
                        tx_id,
                    )
                    return {"tx_id": tx_id[1], "amount": amount, "coin_type": coin_type}
                else:
                    msg = f'You can not withdrown now, try after {os.getenv("time_gap")} hours'
                    return {"error": msg}
            else:
                return {"error": "address prefix is not known"}
                logger.info("address prefix is not known")
        else:
            return {"error": "Adderss is invalid or captcha is wrong"}

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def on(self, secret):
        """Turns the faucet on, requires a secret"""
        if secret == os.getenv("secret"):
            self.faucet_active = True
            return {"success": "Faucet on!"}
        else:
            return {"message": "Wrong password!"}

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def off(self, secret):
        """Turns the faucet off, requires a secret"""
        if secret == os.getenv("secret"):
            self.faucet_active = False
            return {"success": "Faucet off!"}
        else:
            return {"message": "Wrong password!"}

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def status(self):
        """Shows the faucet status"""
        return {"active": self.faucet_active}

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def get_balance(self, secret, network):
        """Get balance of the wallet for the specified network"""
        if secret == os.getenv("secret"):
            balance = ec_slp.get_bch_balance(config_dict[network])
            logger.info(f"get_balance: Balance: {balance}, network: {network}")
            return {"balance": balance, "network": network}
        else:
            return {"message": "Wrong password!"}

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def get_address(self, secret, network):
        """Get an unused address for the specified network"""
        if secret == os.getenv("secret"):
            rpc = config_dict[network]
            # ~ logger.info(f"get_address: rpc: {rpc}")
            address = ec_slp.get_unused_bch_address(rpc)
            logger.info(f"get_address: address: {address}, network: {network}")
            return {"address": address, "network": network}
        else:
            return {"message": "Wrong password!"}

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def set_visual(
        self,
        secret,
        page_h1=page_h1,
        top_logo=top_logo,
        sponsor_title=sponsor_title,
        sponsor_logo=sponsor_logo,
        notice_message=notice_message,
    ):
        """Configure the faucet visiuals"""
        if secret == os.getenv("secret"):
            os.environ["page_h1"] = page_h1
            self.page_h1 = page_h1

            self.top_logo = os.environ["top_logo"] = top_logo

            self.sponsor_title = os.environ["sponsor_title"] = sponsor_title
            
            self.sponsor_logo = os.environ["sponsor_logo"] = sponsor_logo
            
            self.notice_message = os.environ["notice_message"] = notice_message

            logger.info(f"set_visual: page_h1: {page_h1}")
            logger.info(f"set_visual: top_logo: {top_logo}")
            logger.info(f"set_visual: sponsor_title: {sponsor_title}")
            logger.info(f"set_visual: sponsor_logo: {sponsor_logo}")
            logger.info(f"set_visual: notice_message: {notice_message}")

            # Verifiy values stored
            page_h1 = os.getenv("page_h1")
            sponsor_logo = os.getenv("top_logo")
            sponsor_logo = os.getenv("sponsor_title")
            sponsor_logo = os.getenv("sponsor_logo")
            notice_message = os.getenv("notice_message")

            return {
                "page_h1": page_h1,
                "top_logo": top_logo,
                "sponsor_title": sponsor_title,
                "sponsor_logo": sponsor_logo,
                "notice_message": notice_message,
            }
        else:
            return {"message": "Wrong password!"}

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def set_custom_style(self, secret, custom_style=custom_style):
        from urllib.parse import unquote

        if secret == os.getenv("secret"):
            self.custom_style = unquote(custom_style)
            return {"custom_style_length": len(self.custom_style)}
        else:
            return {"message": "Wrong password!"}
        
    @cherrypy.expose
    @cherrypy.tools.json_out()
    def set_amount(self, secret, amount, network=None):
        """set amount desposed by the faucet in BCH"""
        if secret == os.getenv("secret"):
            os.environ["tbch_amount"] = amount
            self.tbch_amount = amount
            self.bch_amount = amount
            # ~ logger.info(f"get_address: rpc: {rpc}")
            env_amount = os.getenv("tbch_amount")
            logger.info(f"set_amount: faucet amount: {env_amount}")
            return {"faucet_amount": env_amount}
        else:
            return {"message": "Wrong password!"}

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def get_amount(self, secret, amount=None, network=None):
        """Get amount desposed by the faucet in BCH"""
        if secret == os.getenv("secret"):
            logger.info(f"get_amount: faucet amount: {self.bch_amount}")
            return {"faucet_amount": self.bch_amount}
        else:
            return {"message": "Wrong password!"}

    @cherrypy.expose
    def admin(self):
        index = (
            open("admin.html")
            .read()
            .format(
                api_url=config_dict["api_url"],
                top_logo=self.top_logo,
                sponsor_title=self.sponsor_title,
                sponsor_logo=self.sponsor_logo,
                page_h1=self.page_h1,
                notice_message=self.notice_message,
                custom_style=self.custom_style,
            )
        )
        return index

    @cherrypy.expose
    def info(self):
        """Gets info about the faucet"""
        with open("info.md", "r") as f:
            text = f.read()
            # Convert Markdown to Html
            html = markdown.markdown(text)
            info = html.format()
        return info

    # greet.exposed=True


###
# Work on additional port
###

# server2 = Server()
# server2.socket_host = "176.56.237.244"
# server2.socket_port = 80
# server2.subscribe()

if __name__ == "__main__":

    # d = Daemonizer(cherrypy.engine)
    # d.subscribe()

    # Drop privileges from root to a different user
    # root is required for deamon running and reading SSL certificates
    # ~ if non_root_run:
    # ~ DropPrivileges(cherrypy.engine,umask=umask, uid=os_uid, gid=os_gid).subscribe()

    # ~ if use_pid_file:
    # ~ PIDFile(cherrypy.engine, app_pid_file).subscribe()

    cherrypy.config.update(
        {
            "log.access_file": config_dict["data_dir"] + config_dict["web_access_log"],
            "log.error_file": config_dict["data_dir"] + config_dict["web_error_log"],
            # ~ 'tools.proxy.base': 'http://tbch.googol.cash'
        }
    )
    cherrypy.quickstart(Form(), "/", config=config_dict["cherrypy_conf"])
