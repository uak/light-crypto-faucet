import os
from distutils.util import strtobool

def get_env_boolean(key):
    string = os.getenv(key)
    return strtobool(string)

