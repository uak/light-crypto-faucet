import pexpect
import sys

# ~ import os

# ~ home_dir = os.path.expanduser('~')

# ~ python_exec = home_dir + "/dev/electron-cash-wallet/venv/bin/python"
# ~ ec_exec = home_dir + "/dev/electron-cash-wallet/src/Electron-Cash/electron-cash"
# ~ network = "scalenet"


def create_wallet_pexpect(python_exec, ec_exec, data_dir, network):
    print("Python: ", python_exec)
    print("Electron Cash: ", ec_exec)
    # Execute the command
    if network != "mainnet":
        p = pexpect.spawn(
            python_exec, [ec_exec, "--dir", data_dir, f"--{network}", "create"]
        )
    else:
        p = pexpect.spawn(
            python_exec, [ec_exec, "--dir", data_dir, "create"]
        )
    # Verbose, show messages
    p.logfile_read = sys.stdout.buffer
    p.delaybeforesend = 2
    print("Exceution done")
    # Expect the response
    p.expect("Password \(hit return if you do not wish to encrypt your wallet\):")
    print("Expected input received")
    p.delaybeforesend = 1
    # Send return key
    p.sendline(b"\r")
    print("Sent response")
    p.expect(pexpect.EOF)
    print("Script done")
