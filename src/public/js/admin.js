function generateURL(api_url, method, amount, secret, network) {
    let url = api_url + method + "?";

    if (amount) {
        url += "amount=" + amount + "&";
    }

    if (secret) {
        url += "secret=" + secret + "&";
    }

    if (network) {
        url += "network=" + network;
    }

    // Remove trailing "&" if present.
    if (url.endsWith("&")) {
        url = url.slice(0, -1);
    }

    return url;
}

function getSecret() {
    const secret = document.getElementById("secret").value;
    return secret;
}

function get_visuals_vlaue() {
    let page_h1 = document.getElementById("page_h1").value;
    let sponsor_logo = document.getElementById("sponsor_logo").value;
    let notice_message = document.getElementById("notice_message").value;
    console.debug("page_h1", page_h1);
    console.debug("sponsor_logo", sponsor_logo);
    console.debug("notice_message", notice_message);
}

function set_visuals_value(api_url) {
    secret = getSecret();
    let top_logo = document.getElementById("top_logo").value;
    let sponsor_title = document.getElementById("sponsor_title").value;
    let sponsor_logo = document.getElementById("sponsor_logo").value;
    let page_h1 = document.getElementById("page_h1").value;
    let notice_message = document.getElementById("notice_message").value;
    let set_visual_url = `${api_url}set_visual?page_h1=${page_h1}&top_logo=${top_logo}&sponsor_title=${sponsor_title}&sponsor_logo=${sponsor_logo}&secret=${secret}&notice_message=${notice_message}`;
    console.debug("set_visual_url", set_visual_url);
    console.debug("notice_message", notice_message);

    fetch_display2(set_visual_url);
}

function set_custom_style(api_url) {
    secret = getSecret();

    let custom_style = document.getElementById("custom_style").value;
    let encoded_custom_style = encodeURIComponent(custom_style);
    let set_custom_url = `${api_url}set_custom_style?custom_style=${encoded_custom_style}&secret=${secret}`;
    console.debug("custom_style", custom_style);
    console.debug("encoded_custom_style", encoded_custom_style);
    console.debug("set_visual_url", set_custom_url);

    fetch_display2(set_custom_url);
}

function get_text_area() {
    let custom_style = document.getElementById("custom_style").value;
    console.log(custom_style);
}

function runQuery(api_url) {
    console.log(api_url);
    const amount = document.getElementById("amount").value;
    secret = getSecret();
    const network = document.getElementById("network").value;
    const method = document.getElementById("method").value;

    return generateURL(api_url, method, amount, secret, network);
}

function show_link(api_url) {
    console.log(api_url);
    const generatedURLstring = runQuery(api_url);
    document.getElementById("result").innerHTML = `
                        <a href=${generatedURLstring}>
                        ${generatedURLstring}
                        </a>`;
}

function getData(api_url) {
    const generatedURLstring = runQuery(api_url);
    fetch_display(generatedURLstring);
}

function get_status(api_url) {
    const query_url = api_url + "status";
    fetch_display(query_url);
}

function switchFaucetOn(api_url) {
    secret = getSecret();
    const on_url = generateURL(api_url, "on", null, secret, null);
    fetch_display(on_url);
}

function switchFaucetOff(api_url) {
    secret = getSecret();
    const on_url = generateURL(api_url, "off", null, secret, null);
    fetch_display(on_url);
}


function fetch_display(query_url) {
    console.debug(document.URL);
    console.debug(window.location.origin);

    fetch(query_url)
        .then(response => response.json())
        .then(data => {
            document.getElementById("other_result").innerHTML = JSON.stringify(data, null, 4);
            console.log(data);
        })
        .catch(error => console.error("Error:", error));
}

function fetch_display2(query_url) {
    console.debug(document.URL);
    console.debug(window.location.origin);

    fetch(query_url)
        .then(response => response.json())
        .then(data => {
            document.getElementById("other_result2").innerHTML = JSON.stringify(data, null, 4);
            console.log(data);
        })
        .catch(error => console.error("Error:", error));
}
