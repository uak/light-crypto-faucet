// Javascript

// Use dark mode based on user preference
if (window.matchMedia &&
    window.matchMedia('(prefers-color-scheme: dark)').matches) {
    document.body.classList.add('dark');
}

// Button for switching dark mode
function switchMode(el) {
    const bodyClass = document.body.classList;
    bodyClass.contains('dark') ?
        (el.innerHTML = '☀️', bodyClass.remove('dark')) :
        (el.innerHTML = '🌙', bodyClass.add('dark'));
}

// When javascript is active, use the button type attribute so javascript
// will handle buttons, otherwise it will be handled by html and redirect
// page to a response page

submit_button = document.getElementById("submit_button")
submit_button.setAttribute('type', 'button');

// Set the URL for queries using `window.location.origin`. 
// Before it used `document.URL` but that lead to issues with hash (#)
// breaking URL

const url = window.location.origin + '/greet' + '?address='
const info_url = window.location.origin + '/info'

console.debug("window.location.origin", window.location.origin)

// Function to submit address and captch and get json object from the backend
async function submit_address_and_captcha() {
    const address = document.getElementById('input__address').value;
    console.debug("address: ", address);
    const captcha = document.getElementById('input__captcha').value;
    console.debug("captcha: ", captcha);
    const network = document.querySelector('input[name="network"]:checked').value;
    console.debug("network: ", network)
    console.debug("network explorer: ", explorer);
    console.debug("network explorer: ", explorer[network]);

    query_url = url + address + "&captcha=" + captcha + "&network=" + network;
    console.log("query_url", query_url)

    let obj = await (await fetch(query_url)).json();
    
    console.debug("response obj: ", obj);

    // Log explorer url
    console.debug(explorer)

    // If we response object has the transaction ID then show success message
    // and disable form
    if ('tx_id' in obj) {
        console.debug("tx_id: ", obj["tx_id"]);
        console.debug("amount: ", obj["amount"]);
        console.debug("coin_type: ", obj["coin_type"]);
        const text = "<br/> ✅ Sent you " + obj["amount"] + " " + obj["coin_type"] + "<a href=" + explorer[network] + obj["tx_id"] + ">" + " You may check on block explorer" + "</a>"
        document.getElementById("info_section").innerHTML = text
        document.getElementById("input__address").disabled = true;
        document.getElementById("input__captcha").disabled = true;
        document.getElementById("submit_button").disabled = true;
    }
    // If no tx_id is found show error message
    else {
        console.debug(obj)
        console.debug(obj["error"])
        document.getElementById("info_section").innerHTML = "<br/>❗" + obj["error"]
    }
}

// Button for element toggling used to show hide info section
const info_button = document.getElementById('toggle');

info_button.onclick = async function() {
    info_obj = await (await fetch(info_url)).text();
    var div = document.getElementById('info');
    if (div.style.display !== 'none') {
        div.style.display = 'none';
    } else {
        div.style.display = 'block';
        document.getElementById("info").innerHTML = info_obj
        document.getElementById("toggle").innerHTML = "Show/Hide info"
        // Move #info to top of screen
        location.hash = "#info";
    }
};


// debug section
//~ function show_debug() {
//~ let network = "testnet"
//~ var debug_div = document.getElementById('debug');
//~ let p = document.createElement("p")
//~ console.log(typeof explorer)
//~ debug_div.append("Some text", explorer, network, explorer[network])
//~ };
