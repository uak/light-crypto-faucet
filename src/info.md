Info
----

You can receive test tBCH using [Electron Cash](https://github.com/Electron-Cash/Electron-Cash) wallet.
Source code: [Light Crypto Faucet](https://gitlab.com/uak/light-crypto-faucet)
For support: [SLP bot group](https://t.me/slpsell_bot)
  
### Explorers

List of explorers:

*   **Testnet3:**
    *   [tbch.loping.net](https://tbch.loping.net)
    *   [testnet2.imaginary.cash](http://testnet2.imaginary.cash/)
    *   [blockchain.com bch testnet](https://www.blockchain.com/tr/explorer?view=bch-testnet)

*   **Testnet4:**
    *   [tbch4.loping.net](https://tbch4.loping.net)
    *   [testnet4.imaginary.cash](http://testnet4.imaginary.cash/)

*   **Chipnet:**
    *   [chipnet.imaginary.cash](https://chipnet.imaginary.cash/)


### Multiple Testnets?

BCH has multiple Testnets, **Testnet3** is the origianl one but it had grown too large because of some excessive testing of large blocks. **Testnet 4** is the newer
**Chipnet** is the new network with the latest rules that are going to be activated in the next network upgrade.
