# syntax=docker/dockerfile-upstream:1-labs

FROM alpine:3.17

MAINTAINER uak@gitlab

EXPOSE 8080

ENV USER_HOME /home/user
ENV DATA_DIR $USER_HOME/data
ENV ENV_FILE $DATA_DIR/.env

RUN apk update
RUN apk --no-cache upgrade
RUN apk add --no-cache python3 libsecp256k1-dev py3-pip logrotate git

RUN cat <<-EOF > /etc/logrotate.d/cherrypy
$DATA_DIR/*.log {
    rotate 12
    weekly
    copytruncate
    compress
    missingok
}
EOF

RUN adduser user --disabled-password --gecos "" --home "$USER_HOME"/
USER user
WORKDIR "$USER_HOME"
RUN mkdir "$DATA_DIR"
VOLUME "$DATA_DIR"

RUN git clone --depth 1 --branch "4.4.1" https://github.com/Electron-Cash/Electron-Cash
WORKDIR Electron-Cash
RUN pip3 install -r contrib/requirements/requirements.txt --user
RUN pip3 install cherrypy peewee ec-slp-lib pexpect markdown python-dotenv captcha

WORKDIR "$USER_HOME"

COPY . light-crypto-faucet/
CMD cp $USER_HOME/light-crypto-faucet/.env $DATA_DIR/.env && \
    cp $USER_HOME/light-crypto-faucet/cherrypy.config.sample $DATA_DIR/cherrypy.config && \
    cd "$USER_HOME"/light-crypto-faucet/src/ && \
    /usr/bin/python3 "$USER_HOME"/light-crypto-faucet/src/main.py

