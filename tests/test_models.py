from peewee import *
from peewee import IntegrityError, OperationalError
from datetime import datetime, timedelta
import os
import tomli

from src.models import (
    Transactions,
    save_transaction,
    recent_user,
    create_table,
    )

from dotenv import dotenv_values
data = dotenv_values()

try:
    database_file = os.getenv("test_database_file", "test_db.sqlite3")
except KeyError:
    exit("Unable to get database_file from environment variables")

# Time gap to determine if user is recent
time_gap = os.getenv("time_gap")

db = SqliteDatabase(None)


def test_create_table():
    create_table(database_file)
    assert Transactions.table_exists()


def test_drop_table():
    Transactions.drop_table()
    assert not Transactions.table_exists()


def test_save_transaction():
    create_table(database_file)
    save_transaction(
        data["cherrypy_session_id"],
        data["cherrypy_request_remote_ip"],
        data["recipient_address"],
        data["coin_type"],
        data["amount"],
        data["transaction_status"],
        data["tx_id"]
    )

    record = Transactions.select(Transactions.session_id).get()
    assert record.session_id == data["cherrypy_session_id"]


def test_recent_user():
    save_transaction(
        data["cherrypy_session_id"],
        data["cherrypy_request_remote_ip"],
        data["recipient_address"],
        data["coin_type"],
        data["amount"],
        data["transaction_status"],
        data["tx_id"]
    )
    recent = recent_user(
        data["cherrypy_session_id"],
        data["cherrypy_request_remote_ip"],
        data["recipient_address"],
        data["coin_type"]
    )
    assert recent


def test_not_recent_user():
    save_transaction(
        data["cherrypy_session_id"],
        data["cherrypy_request_remote_ip"],
        data["recipient_address"],
        data["coin_type"],
        data["amount"],
        data["transaction_status"],
        data["tx_id"]
    )
    recent = recent_user(
        data["cherrypy_session_id"],
        data["cherrypy_request_remote_ip"],
        data["recipient_address"],
        data["other_coin_type"]  # different coin
    )
    assert not recent
